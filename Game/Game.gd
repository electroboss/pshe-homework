extends Node2D


# Declare member variables here.
var questions = [
	[0,"You're feeling lonely.",["Chat with a friend","Watch youtube","Browse reddit"]],
	[2,"You're having trouble going to sleep, and you watch YouTube until 10pm.",["Watch YouTube in bed","Watch YouTube on your television, not your phone","Stop watching YouTube after 8-9pm"]],
	[2,"Someone's sent something that's upset you.",["Delete the message","Block them","Tell an adult"]],
	[1,"You're starting to feel worthless, and you don't see much reason in living.",["Tell yourself you're being stupid","Talk to an adult about it and possibly get a therapist","Play video games all day"]],
	[0,"You're eating less and less in an attempt to get thinner.",["Eat more and eat healthier, and do more exercise.","Continue.","Do some exercise and continue eating less."]],
	[1,"Every day after school, some people in a higher year beat you up.",["Stand up to them","Tell your head of year","Ignore them and live on your life"]],
	[1,"You have no friends or are being isolated.",["Just deal with it","Talk a trusted adult","Try to annoy them to make them stop"]],
	[2,"You're not doing much exercise due to gaming all the time, and you're starting to feel a bit sick.",["Run around the house to do more exercise.","Go running once a week.","Use the computer less often, and spend an hour on walking daily."]],
	[0,"Your parents beat you often, and you've started getting headaches.",["Report it to the police","deal with it","Tell them to stop"]],
	[2,"You're eating a lot, and you start to feel unwell.",["eat less","eat more healthy things","Both of the above"]],
	[2,"You're worried about something.",["Act on the worry without any proof.","Tell yourself you're being stupid.","Talk to someone about it."]],
	[1,"You're really bad at maths, and you've got an exam coming up.",["Give up and don't try to get better.","Try your best and revise a lot.","Cheat on the test."]],
	[0,"You don't do much exercise, and your body aches constantly",["Exercise for at least an hour a day","Exercise once a week","Excercise for 30 minutes 3 days of the week"]]
]
var question = -1
var wait = 0
var nq = false
var score = 0
var q = -1


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	nextQuestion()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	wait -= _delta
	if wait > 0:
		nq=true
	elif nq:
		nextQuestion()
		nq = false


func nextQuestion():
	for x in range(0,3): changeCol(x,0)
	
	q = randi()%(len(questions)-1)
	question = q+1 if q>=question else q
	
	$Foreground/CenterContainer/Options/RichTextLabel.text = questions[question][1]+"\nWhat do you do?"
	$"Foreground/CenterContainer/Options/Option A/Option A".text = questions[question][2][0]
	$"Foreground/CenterContainer/Options/Option B/Option B".text = questions[question][2][1]
	$"Foreground/CenterContainer/Options/Option C/Option C".text = questions[question][2][2]


func checkAnswer(ans):
	if wait <= 0:
		changeCol(questions[question][0],1)
		if ans != questions[question][0]:
			changeCol(ans,2)
			changeCol(3,2)
			score -= 1
		else:
			changeCol(3,1)
			score += 1
		$Foreground/Score.text = "Score: "+str(score)
	if not $Foreground/SkipButton.pressed: wait = 2
	else: wait = 0.1


func changeCol(texrect,col):
	var colpath = "res://Game/texUnselected.tres"
	if col == 0:
		colpath = "res://Game/texUnselected.tres"
	elif col == 1:
		colpath = "res://Game/texCorrect.tres"
	elif col == 2:
		colpath = "res://Game/texIncorrect.tres"
	if texrect == 0:
		$"Foreground/CenterContainer/Options/Option A".texture = load(colpath)
	if texrect == 1:
		$"Foreground/CenterContainer/Options/Option B".texture = load(colpath)
	if texrect == 2:
		$"Foreground/CenterContainer/Options/Option C".texture = load(colpath)
	if texrect == 3:
		$Foreground/TextureRect.texture = load(colpath)
