extends CenterContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$TextureRect.rect_min_size[0] = $Options.rect_size[0]+50
	$TextureRect.rect_min_size[1] = $Options.rect_size[1]+50


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Options_resized():
	$TextureRect.rect_min_size[0] = $Options.rect_size[0]+50
	$TextureRect.rect_min_size[1] = $Options.rect_size[1]+50
